import Footer from 'components/Footer';
import Hero from 'components/Hero';

export default function HomePage() {
	return (
		<div className="min-w-screen max-w-screen flex min-h-screen flex-col">
			<Hero />
			<Footer />
		</div>
	);
}
