import Link from 'next/link';

export default function NavBar() {
	return (
		<div className="min-w-screen flex h-[4.5rem] items-center justify-between bg-hololive px-8 py-4 text-white">
			<Link passHref href="/">
				<h1 className="cursor-pointer text-2xl">Suisei&apos;s Mic</h1>
			</Link>
			<p className="cursor-not-allowed rounded-xl bg-white px-4 py-2 font-bold text-hololive">
				Dashboard
			</p>
		</div>
	);
}
