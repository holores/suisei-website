import { LinkIcon, InformationCircleIcon } from '@heroicons/react/20/solid';

export default function Hero() {
	return (
		<div className="flex h-screen w-full flex-col overflow-x-hidden bg-gradient-to-br from-hololive via-blue-400 to-sky-300 text-white">
			<div className="cloud-bg flex w-full grow flex-col bg-transparent">
				<div className="flex grow flex-col items-center justify-center gap-4 bg-[url('/img/SuiseiSun.png')] bg-cover bg-fixed bg-left-top bg-no-repeat lg:bg-contain lg:bg-right-top">
					<div className="flex w-full flex-col items-center gap-2 bg-gradient-to-r from-transparent via-black/50 to-transparent py-16 px-4 lg:w-8/12">
						<h1 className="text-6xl font-bold">
							Suisei&apos;s Mic
						</h1>
						<h2 className="text-center text-2xl">
							A Discord bot, made for VTuber related servers.
						</h2>
						<p
							className="mt-2 flex cursor-not-allowed items-center gap-2 rounded-lg bg-hololive px-4 py-2 text-2xl"
							title="Disabled! The bot is still under construction, check back later!"
						>
							Go to Dashboard
							<LinkIcon className="h-6 w-6" />
						</p>
					</div>
					<div className="flex items-center justify-center rounded-2xl bg-blue-600 py-4 px-8 lg:w-5/12">
						<div className="flex items-center gap-4">
							<div className="rounded-xl bg-blue-500 p-2">
								<InformationCircleIcon className="h-6 w-6" />
							</div>
							<h2 className="text-xl font-bold lg:text-3xl">
								Currently still under construction
							</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}
