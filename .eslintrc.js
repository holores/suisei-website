module.exports = {
	extends: [
		"airbnb",
		"airbnb-typescript",
		"next/core-web-vitals",
		"plugin:import/recommended",
		"plugin:import/typescript",
		"plugin:react/recommended",
		"plugin:react-hooks/recommended",
		"plugin:jsx-a11y/recommended",
	],
	plugins: ["@typescript-eslint", "import", "jsx-a11y"],
	parser: "@typescript-eslint/parser",
	parserOptions: {
		project: "./tsconfig.json",
	},
	settings: {
		"import/parsers": {
			"@typescript-eslint/parser": [".ts", ".tsx", ".d.ts"],
		},
		"import/resolver": {
			typescript: {
				alwaysTryTypes: true,
				project: "/tsconfig.json",
			},
			node: true,
		},
	},
	rules: {
		"no-tabs": "off",
		indent: "off",
		"sort-keys": "off",
		"prettier/prettier": [
			"off",
			{
				useTabs: false,
			},
		],
		"@typescript-eslint/indent": ["error", "tab"],
		"react/jsx-indent": ["error", "tab"],
		"react/jsx-indent-props": ["error", "tab"],
		"react/react-in-jsx-scope": "off",
		"no-underscore-dangle": "off", // Mongoose uses _id
		"no-plusplus": [
			"error",
			{
				allowForLoopAfterthoughts: true,
			},
		],
		"jsx-a11y/label-has-associated-control": "off",
		"jsx-a11y/anchor-is-valid": [
			"error",
			{
				components: ["Link"],
				specialLink: ["hrefLeft", "hrefRight"],
				aspects: ["invalidHref", "preferButton"],
			},
		],
		"@next/next/no-img-element": "off",
		"import/no-anonymous-default-export": "off",
		"react/require-default-props": "off",
	},
};
